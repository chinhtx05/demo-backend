"use strict";

var _user = require("../model/user.model");

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.create_a_user = function (req, res) {
    var data = req.body;
    if (data) {
        var newUser = new _user2.default({
            Name: data.Name,
            Age: data.Age,
            Adress: data.Adress,
            idScores: data.idScores
        });
        newUser.save(function (err) {
            if (err) {
                return res.send({
                    status: false,
                    msg: "User already exists",
                    error: err
                });
            } else {
                return res.send({
                    status: true,
                    msg: "Create user successfully",
                    data: newUser._id
                });
            }
        });
    } else {
        return res.send({
            status: false,
            msg: "Invalid data"
        });
    }
};
exports.get_all = function (req, res) {
    _user2.default.find({}, function (err, user) {
        if (err) {
            res.send(err);
        }
        res.json({ success: 1, data: user });
    });
};
exports.update_user = function (req, res) {
    _user2.default.findById(req.params.id).exec(function (err, user) {
        if (err) {
            return res.send({
                status: false,
                msg: 'User not found.',
                error: err
            });
        } else {
            var data = req.body;
            user.Name = data.Name;
            user.Age = data.Age;
            user.Adress = data.Adress;

            user.save(function (err) {
                if (err) {
                    res.send(err);
                }
                res.json({
                    status: true,
                    message: 'User info updated successfully'
                });
            });
        }
    });
};
exports.delete_user = function (req, res) {
    _user2.default.deleteOne({
        _id: req.params.id
    }, function (err, User) {
        if (err) {
            res.send(err);
        }
        res.json({ message: "User Successfully Delete" });
    });
};
//# sourceMappingURL=user.controller.js.map