'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _user = require('../model/user.model');

var _user2 = _interopRequireDefault(_user);

var _point = require('../model/point.model');

var _point2 = _interopRequireDefault(_point);

var _express = require('express');

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var config = _ref.config,
        db = _ref.db;

    var api = (0, _express.Router)();

    api.get('/', function (req, res) {
        _point2.default.find({}, function (err, point) {
            if (err) {
                res.send(err);
            }
            res.json({ success: 1, data: point });
        });
    });

    api.get('/:id', function (req, res) {
        _point2.default.find(req.params.idUser, function (err, point) {
            if (err) {
                res.send(err);
            }
            res.json({ success: 1, data: point });
        });
    });
    api.delete('/:id', function (req, res) {
        _point2.default.remove({
            _id: req.params.id
        }, function (err, User) {
            if (err) {
                res.send(err);
            }
            res.json({ message: "Point Successfully Removed" });
        });
    });

    api.post('/add', function (req, res) {
        if (req.body) {
            var data = req.body;
            var newPoint = new _point2.default({
                idUser: data.idUser,
                monhoc: data.monhoc,
                point: data.point
            });
            _user2.default.findById(data.idUser, function (err, user) {
                if (err) {
                    return res.send({
                        status: false,
                        msg: 'user not found',
                        error: err
                    });
                } else {
                    user.idPoint.push(newPoint._id);
                    _user2.default.updateOne({ _id: user._id }, user, function (err, st) {
                        if (err) {
                            return res.send({
                                status: false,
                                msg: "can't save user",
                                error: err
                            });
                        } else {
                            newPoint.idUser = user._id;
                            newPoint.save(function (err) {
                                if (err) {
                                    return res.send({
                                        status: false,
                                        msg: "can't save point",
                                        error: err
                                    });
                                } else {
                                    return res.send({
                                        status: true,
                                        msg: "created point successfully",
                                        data: newPoint
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            return res.send({
                status: false,
                msg: 'Invalid data'
            });
        }
    });

    return api;
};
//# sourceMappingURL=point.controler.js.map