'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;
var ScoresSchema = new Schema({
    Name: {
        type: String
    },
    idScores: [{
        type: Schema.Types.ObjectId,
        ref: 'scores'
    }],
    Age: {
        type: String
    },
    Adress: {
        type: String,
        required: true
    },
    role: {
        type: Schema.Types.Number,
        default: 0
    }

});
exports.default = _mongoose2.default.model('User', ScoresSchema);
//# sourceMappingURL=user.model.js.map