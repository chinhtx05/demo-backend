'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;
var ScoresSchema = new Schema({
    idUser: [{
        type: Schema.Types.ObjectId,
        ref: 'users'
    }],
    codeSubject: {
        type: String,
        required: 'Enter the codeSubject'
    },
    points: {
        type: Number,
        required: true
    }
});

exports.default = _mongoose2.default.model('scores', ScoresSchema);
//# sourceMappingURL=scores.model.js.map