'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;
var PointSchema = new Schema({
    monhoc: {
        type: String,
        required: true
    },
    idUser: [{
        type: Schema.Types.ObjectId,
        ref: 'user'
    }],
    point: {
        type: String,
        required: true
    }
});

exports.default = _mongoose2.default.model('point', PointSchema);
//# sourceMappingURL=point.model.js.map