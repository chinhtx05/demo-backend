'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _user = require('../controller/user.controller');

var _user2 = _interopRequireDefault(_user);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('../config/passport')(_passport2.default);

exports.default = function (_ref) {
    var config = _ref.config,
        db = _ref.db;

    var api = (0, _express.Router)();
    //create user
    api.post('/register', _user2.default.create_a_user);

    // login
    api.get('/all', _user2.default.get_all);

    // // update
    api.put('/:id', _user2.default.update_user);

    //delete
    api.delete('/:id', _user2.default.delete_user);

    return api;
};
//# sourceMappingURL=userRoute.js.map