'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _scores = require('../controller/scores.controller');

var _scores2 = _interopRequireDefault(_scores);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('../config/passport')(_passport2.default);

exports.default = function (_ref) {
    var config = _ref.config,
        db = _ref.db;

    var api = (0, _express.Router)();

    // api.get('/all/:id',ctrl.get_all_scores)
    //create user
    api.post('/create', _scores2.default.create_a_scores);

    // // update
    api.put('/:id', _scores2.default.update_a_scores);

    api.get('/all', _scores2.default.get_all);

    return api;
};
//# sourceMappingURL=scoresRoute.js.map