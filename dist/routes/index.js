'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

var _db = require('../db');

var _db2 = _interopRequireDefault(_db);

var _userRoute = require('./userRoute');

var _userRoute2 = _interopRequireDefault(_userRoute);

var _scoresRoute = require('./scoresRoute');

var _scoresRoute2 = _interopRequireDefault(_scoresRoute);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = (0, _express2.default)();

// connect to db
(0, _db2.default)(function (db) {
    // router.use(middleware({ config, db }));

    // api routes user v1 (/v1)
    router.use('/user', (0, _userRoute2.default)({ config: _config2.default, db: db }));
    // api routes mail v1 (/v1)
    router.use('/scores', (0, _scoresRoute2.default)({ config: _config2.default, db: db }));
});

exports.default = router;
//# sourceMappingURL=index.js.map