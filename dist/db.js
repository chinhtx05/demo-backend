'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _database = require('./config/database');

var _database2 = _interopRequireDefault(_database);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (callback) {
  // let db = mongoose.connect('mongodb://CH4:Teatea94@ds143532.mlab.com:43532/usernihon');
  _mongoose2.default.Promise = global.Promise;
  var db = _mongoose2.default.connect(_database2.default.database);
  callback(db);
};
//# sourceMappingURL=db.js.map