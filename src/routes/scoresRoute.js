import {Router} from 'express'
import ctrl from '../controller/scores.controller'
import passport from 'passport'
require('../config/passport')(passport);
export default ({config,db}) => {
    let api = Router()

    // api.get('/all/:id',ctrl.get_all_scores)
    //create user
    api.post('/create',ctrl.create_a_scores)

    // // update
    api.put('/:id',ctrl.update_a_scores)

    api.get('/all',ctrl.get_all)

    return api
}