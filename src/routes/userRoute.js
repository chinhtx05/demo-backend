import {Router} from 'express'
import ctrl from '../controller/user.controller'
import passport from 'passport'
require('../config/passport')(passport);
export default ({config,db}) => {
    let api = Router()
    //create user
    api.post('/register',ctrl.create_a_user)

    // login
    api.get('/all',ctrl.get_all)

    // // update
    api.put('/:id',ctrl.update_user)

    //delete
    api.delete('/:id',ctrl.delete_user)

    return api
}