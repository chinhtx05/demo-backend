import express from 'express';
import config from '../config';
import initializeDb from '../db';
import user from './userRoute'
import scores from './scoresRoute'
let router = express();

// connect to db
initializeDb(db => {
    // router.use(middleware({ config, db }));

    // api routes user v1 (/v1)
    router.use('/user', user({ config, db }));
      // api routes mail v1 (/v1)
    router.use('/scores', scores({ config, db }));

});

export default router;
