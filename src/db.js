import mongoose from 'mongoose';
import database from './config/database'
export default callback => {
  // let db = mongoose.connect('mongodb://CH4:Teatea94@ds143532.mlab.com:43532/usernihon');
    mongoose.Promise = global.Promise;
    let db = mongoose.connect(database.database);
    callback(db);
}
