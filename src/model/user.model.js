import mongoose from 'mongoose';
let Schema  = mongoose.Schema;
let ScoresSchema = new Schema({
    Name : {
        type : String
    },
    idScores : [{
        type : Schema.Types.ObjectId,
        ref : 'scores'
    }],
    Age :  {
        type : String
    },
    Adress : {
        type : String,
        required: true
    },
    role : {
        type : Schema.Types.Number,
        default : 0
    }
   
});
export default mongoose.model('User', ScoresSchema);