import mongoose from 'mongoose';
let Schema  = mongoose.Schema;
let ScoresSchema = new Schema({
    idUser :  [{
        type : Schema.Types.ObjectId,
        ref : 'users'
    }],
    codeSubject :{
        type : String,
        required: 'Enter the codeSubject'
    },
    points : {
        type : Number,
        required: true
      },
});

export default mongoose.model('scores', ScoresSchema);