import User from '../model/user.model'
import Scores from '../model/scores.model'
exports.create_a_scores = (req, res) => {
    if (req.body) {
        let data = req.body;
        let newScores = new Scores({
            idUser: data.idUser,
            codeSubject: data.codeSubject,
            points: data.points
        });
        User.findById(data.idUser, (err, user) => {
            if (err) {
                console.log(err)
            } else {
                newScores.save(err => {
                    if (err) {
                        console.log(err)
                    } else {
                        user.idScroses = newScores._id;
                        User.updateOne({ _id: user._id }, user, (err, st) => {
                            if (err) {
                                console.log(err)
                            } else {                  
                                return res.send({
                                    status: true,
                                    msg: "created scores successfully",
                                    data: newScores
                                })
                            }
                        })
                    }
                });
            }
        });
    } else {
        console.log(err)
    }
}
exports.get_all = (req, res) => {
    Scores.find({}, (err, scores) => {
        if (err) {
            res.send(err);
        }
        res.json({ success: 1, data: scores });
    });
};
exports.update_a_scores = (req, res) => {
    if (req.body) {
        Scores.findById(req.params.id).exec((err, scores) => {
            console.log('chinh id', scores)
            if (err) {
                return res.send({
                    status: false,
                    msg: 'Scores not found.',
                    error: err
                })
            } else {
                let data = req.body;
                // scores.idUser = data.idUser
                scores.codeSubject = data.codeSubject
                scores.points = data.points

                scores.save(function (err) {
                    if (err) {
                        res.send(err);
                    }
                    res.json({
                        status: true,
                        message: 'Scores info updated successfully'
                    });
                });

            }
        })
    }
}