import User from '../model/user.model'

exports.create_a_user = (req, res) => {
    let data = req.body;
    if (data) {
        let newUser = new User({
            Name: data.Name,
            Age: data.Age,
            Adress: data.Adress,
            idScores: data.idScores
        })
        newUser.save(err => {
            if (err) {
                return res.send({
                    status: false,
                    msg: "User already exists",
                    error: err
                })
            } else {
                return res.send({
                    status: true,
                    msg: "Create user successfully",
                    data: newUser._id
                })
            }
        })
    } else {
        return res.send({
            status: false,
            msg: "Invalid data"
        })
    }
}
exports.get_all = (req, res) => {
    User.find({}, (err, user) => {
        if (err) {
            res.send(err);
        }
        res.json({ success: 1, data: user });
    });
};
exports.update_user = (req, res) => {
    User.findById(req.params.id).exec((err, user) => {
        if (err) {
            return res.send({
                status: false,
                msg: 'User not found.',
                error: err
            })
        } else {
            let data = req.body;
            user.Name = data.Name
            user.Age = data.Age
            user.Adress = data.Adress

            user.save(function (err) {
                if (err) {
                    res.send(err);
                }
                res.json({
                    status: true,
                    message: 'User info updated successfully'
                });
            });

        }
    })
}
exports.delete_user = (req, res) => {
    User.deleteOne({
        _id: req.params.id
    }, (err, User) => {
        if (err) {
            res.send(err);
        }
        res.json({ message: "User Successfully Delete" });
    });
};
